package src;

import java.util.Scanner;

public class Lab01 {
   
   public void start(String[] args){
      homework();
   }
   public static void homework(){
      Scanner sc = new Scanner(System.in);
      System.out.print("Vyber operaci (1-soucet, 2-rozdil, 3-soucin, 4-podil):");
      int operation = sc.nextInt();
      if (operation == 1){
         System.out.print("Zadej scitanec: ");
         float plus = sc.nextFloat();
         System.out.print("Zadej scitanec: ");
         float subplus = sc.nextFloat();
         System.out.print("Zadej pocet desetinnych mist: ");
         int point = sc.nextInt();
         boolean check = controlle(point);
         if (check==false){
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            return;
         }else{
            float equal = plus+subplus;
            char sign = '+';
            printer(plus,subplus,sign,equal,point);
            return;
         }
      }else if(operation==3) {
         System.out.print("Zadej cinitel: ");
         float multi = sc.nextFloat();
         System.out.print("Zadej cinitel: ");
         float submulti = sc.nextFloat();
         System.out.print("Zadej pocet desetinnych mist: ");
         int point = sc.nextInt();
         boolean check = controlle(point);
         if (check==false){
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            return;
         }else{
            float equal = multi*submulti;
            char sign = '*';
            printer(multi,submulti,sign,equal,point);
            return;
         }

      }else if(operation==2) {
         System.out.print("Zadej mensenec: ");
         float factor = sc.nextFloat();
         System.out.print("Zadej mensitel: ");
         float subfactor = sc.nextFloat();
         System.out.print("Zadej pocet desetinnych mist: ");
         int point = sc.nextInt();
         boolean check = controlle(point);
         if (check == false) {
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            return;
         }else{
            float equal = factor - subfactor;
            char sign = '-';
            printer(factor,subfactor,sign,equal,point);
            return;
         }
      }else if(operation==4) {
         System.out.print("Zadej delenec: ");
         float del = sc.nextFloat();
         System.out.print("Zadej delitel: ");
         float subdel = sc.nextFloat();
         if (subdel==0){
            System.out.println("Pokus o deleni nulou!");
            return;
         }
         System.out.print("Zadej pocet desetinnych mist: ");
         int point = sc.nextInt();
         boolean check = controlle(point);
         if (check==false){
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            return;
         }else{
            float equal = del/subdel;
            char sign = '/';
            printer(del,subdel,sign,equal,point);
            return;
         }
      }else{
            System.out.println("Chybna volba!");
            return;
         }
   }

   private static void printer(float first, float second, char sigh, float equal,int point){
      String f = "%."+point+"f %c %."+point+"f = %."+point+"f";
      System.out.println(String.format(f,first,sigh,second,equal));
      System.out.printf();
   }
   private static boolean controlle(int point){
      if (point<0){
         return false;
      }
      return true;
   }
}